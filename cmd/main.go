package main

import (
	_ "github.com/lib/pq"
	canada "github.com/symetime/candserv"
	"github.com/symetime/candserv/package/handler"
	"github.com/symetime/candserv/package/repository"
	"github.com/symetime/candserv/package/service"
	"log"
)

func main() {
	db, err := repository.NewPostgresDB(repository.Config{
		Host:     "",
		Port:     "",
		Username: "",
		Password: "",
		DBName:   "",
		SSLMode:  "",
	})
	if err != nil {
		log.Fatalf("failed to initialize db, %s", err.Error())
	}

	repos := repository.NewRepository(db)
	services := service.NewService(repos)
	handlers := handler.NewHandler(services)

	srv := new(canada.Server)
	if err := srv.Run("8800", handlers.InitRoutes()); err != nil {
		log.Fatalf("error while running http service,  %s", err)
	}
}
