package canada

type User struct {
	Id       int    `json:"-"`
	Name     string `json:"username"`
	Password string `json:"password"`
}
